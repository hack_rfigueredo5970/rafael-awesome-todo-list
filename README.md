# Rafael Awesome Todo List

## Description

A simple to-do list application with a backend API built using Node.js, Express, and TypeScript, and a React frontend runing on Vite.

## Table of Contents

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Running the Application](#running-the-application)
  - [Running the tests](#running-the-application)
- [Usage](#usage)
- [Author](#author)
- [License](#license)

## Getting Started

### Prerequisites

- [Node.js](https://nodejs.org/) (v16.20.2 (LTS) or later)
- [npm](https://www.npmjs.com/) (v8.19.4 or later)

### Installation

1. Clone the repository:

   ```bash
    git clone https://gitlab.com/hack_rfigueredo5970/rafael-awesome-todo-list.git
    cd rafael-awesome-todo-list

2. Install backend dependencies

    ```bash
     cd backend
     npm install

3. Install frontend dependencies

      ```bash
       cd ../frontend
       npm install

## Running the Application

1. Start the backend server:

   ```bash
    cd backend 
    npm run dev

The server will be running at <http://localhost:3000>

2. Start the frontend server:

   ```bash
    cd frontend 
    npm run dev

The server will be running at <http://localhost:5173>

## Running the Tests

1. Run the backend tests:

   ```bash
    cd backend 
    npm run test

2. Run the frontend tests:

   ```bash
    cd frontend 
    npm run test

## Usage

Click the *Create Task* button to add tasks to the list. Switch the toggle to mark the task as *completed*. Click the  trash icon to delete it

## Author

Rafael Figueredo - <refo44@gmail.com>

## License

This is an open source project, "license": "ISC"
