import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import TaskList from './components/TaskList';
import NotFound from './components/NotFound';
import './App.css';

function App() {
  return (
    <div className="App">
    <Router>
      <Routes>
        <Route path="/" element={<TaskList />} />
        <Route path="/tasks" element={<TaskList />} />
        <Route path="*" element={<NotFound />} /> {/* Add a NotFound component */}
      </Routes>
    </Router>
    </div>
  );
}

export default App;
