// src/handlers/taskHandlers.js

import axios from 'axios';
import { UUID } from '../utils/interfaces/UUID';
import { Dispatch, SetStateAction } from 'react';
import { ITask } from '../utils/interfaces/ITask';

export const deleteTask = async (taskId: UUID, setTasks: Dispatch<SetStateAction<ITask[]>>) => {
  try {
    await axios.delete(`http://localhost:3000/api/tasks/${taskId}`);
    setTasks((prevTasks) => prevTasks.filter((task) => task.id !== taskId));
  } catch (error) {
    console.error('Error deleting task:', error);
  }
};


export const createTask = async (formData: Partial<ITask>, setTasks: Dispatch<SetStateAction<ITask[]>>) => {
    try {
  
      const response = await axios.post('http://localhost:3000/api/tasks', formData);
      setTasks((prevTasks) => [...prevTasks, response.data]);
    } catch (error) {
      console.error('Error creating task:', error);
    }
  };

  // Change The task to complete
export const toggleCompleteTask  = async (taskId: UUID, tasks: ITask[], setTasks: Dispatch<SetStateAction<ITask[]>>) => {
    try {
        await axios.patch(`http://localhost:3000/api/tasks/${taskId}`, {
          completed: !tasks?.find((task) => task.id === taskId)?.completed,
        });
        setTasks((prevTasks) =>
          prevTasks.map((task) =>
            task.id === taskId
              ? { ...task, completed: !task.completed }
              : task
          )
        );
      } catch (error) {
        console.error('Error updating task:', error);
      }
  };
  
