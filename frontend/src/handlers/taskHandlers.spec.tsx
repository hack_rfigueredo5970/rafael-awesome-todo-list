import axios from 'axios';
import {
  deleteTask,
  createTask,
  toggleCompleteTask,
} from './taskHandlers'; // Adjust the import path as needed
import { ITask } from '../utils/interfaces/ITask';

// Mock axios.delete and axios.post
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;


const fakeDate = new Date();
const mockTask: ITask = {
  title: 'Test Task',
  description: 'Test Description',
  completed: false,
  createdBy: 'fakeUserId',
  createdAt: fakeDate,
  updatedAt: fakeDate,
};


describe('Task Handlers', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should delete a task', async () => {
    const taskId = '123';
    const setTasksMock = jest.fn();

    mockedAxios.delete.mockResolvedValueOnce({}); // Mock axios.delete

    await deleteTask(taskId, setTasksMock);

    expect(mockedAxios.delete).toHaveBeenCalledWith(
      `http://localhost:3000/api/tasks/${taskId}`
    );
  });

  it('should create a task', async () => {
    const formData = {
      title: 'New Task',
      // Add other properties as needed
    };
    const newTask: ITask = { ...mockTask, id: '789', ...formData };
    const setTasksMock = jest.fn();

    mockedAxios.post.mockResolvedValueOnce({ data: newTask }); // Mock axios.post

    await createTask(formData, setTasksMock);

    expect(mockedAxios.post).toHaveBeenCalledWith(
      'http://localhost:3000/api/tasks',
      formData
    );
  });

  it('should toggle task completion', async () => {
    const taskId = '123';
    const mockTasks: ITask[] = [
      { ...mockTask, id: '123', title: 'Task 1', completed: false },
      {...mockTask,  id: '456', title: 'Task 2', completed: true },
    ];
    const setTasksMock = jest.fn();

    mockedAxios.patch.mockResolvedValueOnce({}); // Mock axios.patch

    await toggleCompleteTask(taskId, mockTasks, setTasksMock);

    expect(mockedAxios.patch).toHaveBeenCalledWith(
      `http://localhost:3000/api/tasks/${taskId}`,
      { completed: !mockTasks[0].completed }
    );
  });
});
