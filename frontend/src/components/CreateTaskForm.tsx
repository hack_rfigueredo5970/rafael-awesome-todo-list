import { Dispatch, SetStateAction, useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { createTask } from '../handlers/taskHandlers';
import { ITask } from '../utils/interfaces/ITask';

interface CreateTaskFormProps {
    setTasks: Dispatch<SetStateAction<ITask[]>>;// Define the onCancel prop// Define the setTasks prop with correct type
}

function CreateTaskForm({ setTasks  }: CreateTaskFormProps) {
  const [open, setOpen] = useState(true);
  const [isTitleEmpty, setIsTitleEmpty] = useState(false);
  const [formData, setFormData] = useState({
    title: '',
    description: '',
    completed: false,
    createdBy: 'YourUser', 
  });

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
    if (name === 'title') {
        setIsTitleEmpty(!value?.toString()?.trim());
    } 
  };

  const handleSubmit = () => {  
    if (!formData.title?.toString()?.trim() || isTitleEmpty) {
        setIsTitleEmpty(true);
        return
    } else  {
        setIsTitleEmpty(false);
    }
    // Send a POST request to create a new task
    createTask(formData, setTasks); // Pass formData and setTasks to the createTask function
    handleClose();

// Reset the formData state
  setFormData({
    title: '',
    description: '',
    completed: false,
    createdBy: 'YourUser', // You can set the default user here
  });
  };

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleOpen}>
        Create Task
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Create a New Task</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Fill in the details for the new task.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            name="title"
            label="Title"
            type="text"
            error={isTitleEmpty} 
            helperText={isTitleEmpty ? 'Title is required' : ''} 
            required 
            fullWidth
            onChange={handleChange}
          />
          <TextField
            margin="dense"
            name="description"
            label="Description"
            type="text"
            fullWidth
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default CreateTaskForm;
