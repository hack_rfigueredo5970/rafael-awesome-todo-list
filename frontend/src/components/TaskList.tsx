import { useEffect, useState } from 'react';
import axios from 'axios';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import { deleteTask, toggleCompleteTask } from '../handlers/taskHandlers'; 
import { UUID } from '../utils/interfaces/UUID';
import { ITask } from '../utils/interfaces/ITask';
import ListItemIcon from '@mui/material/ListItemIcon/ListItemIcon';
import WatchLaterTwoToneIcon from '@mui/icons-material/WatchLaterTwoTone';
import Switch from '@mui/material/Switch/Switch';
import './styles/TaskList.css'; // Import the CSS file
import Button from '@mui/material/Button/Button';
import CreateTaskForm from './CreateTaskForm'; 

function TaskList() {
  const [tasks, setTasks] = useState<ITask[]>([]);
  const [isCreatingTask, setIsCreatingTask] = useState(false); 

  useEffect(() => {
    axios.get('http://localhost:3000/api/tasks')
      .then((response) => {
        setTasks(response.data);
      })
      .catch((error) => {
        console.error('Error fetching tasks:', error);
      });
  }, [isCreatingTask]);

  const handleDelete = (taskId: UUID) => {
    deleteTask(taskId, setTasks);
  };


  const handleToggleComplete = async (taskId:UUID) => {
   // Send a request to update the task as completed or not
    toggleCompleteTask (taskId, tasks , setTasks);
  };

  return (
    <div>
      <h1>Rafa's Awesome Task List!</h1>
      {!isCreatingTask && (
    
      <Button
        variant="contained"
        color="primary"
        onClick={() => setIsCreatingTask(true)} // Call the form to create a task
      >
        Create Task
      </Button>
        )}
        {/* Render the task creation form when isCreatingTask is true */}
        {isCreatingTask && (
        <CreateTaskForm setTasks={setTasks} />
      )}
      <List>
        {tasks.map((task) => (
          <ListItem key={task.id}>
            <ListItemIcon>
          <WatchLaterTwoToneIcon  className={task.completed ? 'completed-icon' : ''}/>
        </ListItemIcon>
            <ListItemText
              primary={task.title}
              secondary={task.description}
        />
        <Switch
            edge="end"
            onChange={() => handleToggleComplete(task.id || '')} //
            checked={task.completed} 
            inputProps={{
            'aria-labelledby': 'switch-list-label-completed',
          }}
        />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={() => handleDelete(task?.id || '')}
              >
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </div>
  );
}

export default TaskList;
