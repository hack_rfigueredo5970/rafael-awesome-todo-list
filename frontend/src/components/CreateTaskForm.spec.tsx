import { render, screen, fireEvent } from '@testing-library/react';
import CreateTaskForm from './CreateTaskForm';

jest.mock('../handlers/taskHandlers', () => ({
  createTask: jest.fn(),
}));

describe('CreateTaskForm', () => {
  it('should render the Create Task button', () => {
    render(<CreateTaskForm setTasks={() => {}} />);
    const createTaskButton = screen.getByText('Create Task');
    expect(createTaskButton).toBeTruthy();
  });

  it('should open the dialog when Create Task button is clicked', () => {
    render(<CreateTaskForm setTasks={() => {}} />);
    const createTaskButton = screen.getByText('Create Task');
    fireEvent.click(createTaskButton);
    const dialogTitle = screen.getByText('Create a New Task');
    expect(dialogTitle).toBeTruthy();
  });

  it('should show an error when trying to submit with an empty title', () => {
    render(<CreateTaskForm setTasks={() => {}} />);
    const createTaskButton = screen.getByText('Create Task');
    fireEvent.click(createTaskButton);

    // Simulate form submission with an empty title
    const submitButton = screen.getByText('Create');
    fireEvent.click(submitButton);

    // Assert that the error message is displayed
    const errorText = screen.getByText('Title is required');
    expect(errorText).toBeTruthy();
  });
});
