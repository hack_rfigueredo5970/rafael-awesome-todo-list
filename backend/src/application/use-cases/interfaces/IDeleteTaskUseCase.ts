import { UUID } from '../../../utils/types/UUID';

export interface IDeleteTaskUseCase {
  execute(taskId: UUID): Promise<boolean>;
}
