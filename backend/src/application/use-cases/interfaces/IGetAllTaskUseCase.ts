import { ITask } from '../../../domain/entities/interfaces/ITask';

export interface IGetAllTaskUseCase {
  execute(): Promise<ITask[]>;
}
