import { ITask } from '../../../domain/entities/interfaces/ITask';

export interface IGetTaskUseCase {
  execute(taskId: string): Promise<ITask | null>;
}
