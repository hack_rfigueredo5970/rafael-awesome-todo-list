import { ITask } from '../../../domain/entities/interfaces/ITask';

export interface IUpdateTaskUseCase {
  execute(updatedTaskData: ITask): Promise<ITask | null>;
}
