import { ITask } from '../../../domain/entities/interfaces/ITask';

export interface ICreateTaskUseCase {
  execute(taskData: ITask): Promise<ITask>;
}
