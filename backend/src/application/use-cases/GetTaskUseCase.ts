import { IGetTaskUseCase } from './interfaces/IGetTaskUseCase';
import { ITaskRepository } from '../../domain/repositories/interfaces/ITaskRepository';
import { ITask } from '../../domain/entities/interfaces/ITask';

export class GetTaskUseCase implements IGetTaskUseCase  {
  constructor(private readonly taskRepository: ITaskRepository) {}

  async execute(taskId: string): Promise<ITask | null> {
    const task = await this.taskRepository.getTaskById(taskId);
    return task;
  }
}
