import { ITaskRepository } from '../../domain/repositories/interfaces/ITaskRepository';
import { ITask } from '../../domain/entities/interfaces/ITask';
import { ICreateTaskUseCase } from './interfaces/ICreateTaskUseCase';

export class CreateTaskUseCase implements ICreateTaskUseCase {
  constructor(private readonly taskRepository: ITaskRepository) {}

  async execute(taskData: ITask): Promise<ITask> {
    const createdTask = await this.taskRepository.createTask(taskData);
    return createdTask;
  }
}
