import { ITaskRepository } from '../../domain/repositories/interfaces/ITaskRepository';
import { IDeleteTaskUseCase } from './interfaces/IDeleteTaskUseCase';

export class  DeleteTaskUseCase implements IDeleteTaskUseCase {
  constructor(private readonly taskRepository: ITaskRepository) {}

  async execute(taskId: string): Promise<boolean> {
    const isDeleted = await this.taskRepository.deleteTask(taskId);
    return isDeleted;
  }
}
