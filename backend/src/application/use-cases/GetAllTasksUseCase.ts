import { ITaskRepository } from '../../domain/repositories/interfaces/ITaskRepository';
import { ITask } from '../../domain/entities/interfaces/ITask';
import { IGetAllTaskUseCase } from './interfaces/IGetAllTaskUseCase';

export class GetAllTasksUseCase implements IGetAllTaskUseCase   {
  constructor(private readonly taskRepository: ITaskRepository) {}

  async execute(): Promise<ITask[]> {
    const tasks = await this.taskRepository.getAllTasks();
    return tasks;
  }
}
