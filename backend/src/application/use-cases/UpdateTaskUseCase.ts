import { IUpdateTaskUseCase } from './interfaces/IUpdateTaskUseCase';
import { ITaskRepository } from '../../domain/repositories/interfaces/ITaskRepository';
import { ITask } from '../../domain/entities/interfaces/ITask';

export class UpdateTaskUseCase implements IUpdateTaskUseCase  {
  constructor(private readonly taskRepository: ITaskRepository) {}

  async execute(updatedTaskData: ITask): Promise<ITask | null> {
    const updatedTask = await this.taskRepository.updateTask(updatedTaskData);
    return updatedTask;
  }
}
