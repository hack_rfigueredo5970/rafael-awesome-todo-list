import { ITaskRepository } from '../../domain/repositories/interfaces/ITaskRepository';
import { SimpleTaskRepository } from '../persistence/simple/SimpleTaskRepository';

export enum DatabaseType {
  SIMPLE = 'simple',
}

export function createTaskRepository(databaseType: DatabaseType): ITaskRepository {
  switch (databaseType) {
    case DatabaseType.SIMPLE:
      return new SimpleTaskRepository();
    default:
      throw new Error('Invalid database type specified');
  }
}
