import { SimpleTaskRepository } from './SimpleTaskRepository';
import { ITask } from '../../../domain/entities/interfaces/ITask';

describe('SimpleTaskRepository', () => {
  let taskRepository: SimpleTaskRepository;
  const fakeDate = new Date();
  

  beforeEach(() => {
    taskRepository = new SimpleTaskRepository();
  });

  it('should create a new task', async () => {
    const newTask: ITask = {
      id: '',
      title: 'Test Task',
      description: 'Test Description',
      completed: false,
      createdBy: 'fakeUserId',
      createdAt: fakeDate,
      updatedAt: fakeDate,
    };

    const createdTask = await taskRepository.createTask(newTask);

    expect(createdTask.id).toBeTruthy();
    expect(createdTask.title).toBe('Test Task');
    expect(createdTask.description).toBe('Test Description');
  });

  it('should retrieve all tasks', async () => {
    const tasks = await taskRepository.getAllTasks();

    expect(Array.isArray(tasks)).toBe(true);
    expect(tasks.length).toBe(0);
  });

  it('should retrieve a task by ID', async () => {
    const newTask: ITask = {
      id: '',
      title: 'Test Task',
      description: 'Test Description',
      completed: false,
      createdBy: 'fakeUserId',
      createdAt: fakeDate,
      updatedAt: fakeDate,
    };

    const createdTask = await taskRepository.createTask(newTask);

    if (createdTask.id) {
      const retrievedTask = await taskRepository.getTaskById(createdTask.id);
      expect(retrievedTask).toBeDefined();
      expect(retrievedTask?.id).toBe(createdTask.id);
    } else {
      fail('Task ID is undefined');
    }
  });

  it('should update an existing task', async () => {
    const newTask: ITask = {
      id: '',
      title: 'Test Task',
      description: 'Test Description',
      completed: false,
      createdBy: 'fakeUserId',
      createdAt: fakeDate,
      updatedAt: fakeDate,
    };

    const createdTask = await taskRepository.createTask(newTask);

    const updatedTask: ITask = {
      ...createdTask,
      title: 'Updated Task',
      description: 'Updated Description',
    };

    const updatedTaskResult = await taskRepository.updateTask(updatedTask);

    expect(updatedTaskResult).toBeDefined();
    expect(updatedTaskResult?.title).toBe('Updated Task');
    expect(updatedTaskResult?.description).toBe('Updated Description');
  });

  it('should delete an existing task', async () => {
    const newTask: ITask = {
      id: '',
      title: 'Test Task',
      description: 'Test Description',
      completed: false,
      createdBy: 'fakeUserId',
      createdAt: fakeDate,
      updatedAt: fakeDate,
    };

    const createdTask = await taskRepository.createTask(newTask);
    if (createdTask.id) {
      const deleteResult = await taskRepository.deleteTask(createdTask.id);
      expect(deleteResult).toBe(true);
    } else {
      fail('Task ID is undefined');
    }
   
  });
});
