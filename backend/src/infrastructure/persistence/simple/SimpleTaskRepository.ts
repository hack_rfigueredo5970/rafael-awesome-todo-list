// libs
import { v4 as uuidv4 } from 'uuid'; 


// interfaces
import { ITask } from '../../../domain/entities/interfaces/ITask';
import { ITaskRepository } from '../../../domain/repositories/interfaces/ITaskRepository';

//types
import { UUID } from '../../../utils/types/UUID';
import { IDictionary } from '../../../utils/interfaces/IDictionary';

export class SimpleTaskRepository implements ITaskRepository {
  private tasks: IDictionary<ITask> = {};
  
  getAllTasks = async ():  Promise<ITask[]> => {
    return Object.values(this.tasks);
  };
  
  getTaskById = async (id: UUID): Promise<ITask | null>  => {
    return this.tasks[id] || null;
  };
  
  createTask = async (newTask: ITask):  Promise<ITask> => {
    newTask.id = this.generateUniqueId();
    this.tasks[newTask.id] = newTask;
    return newTask;
  };
  
  updateTask = async (updatedTask: ITask): Promise<ITask | null>  => {
    const id = updatedTask?.id;
    if (id  && Object.prototype.hasOwnProperty.call(this.tasks, id)) {
      this.tasks[id] = updatedTask;
      return updatedTask;
    }

    return null; // Task not found
  };
  
  deleteTask  = async (id: UUID): Promise<boolean> =>  {
    const initialLength = Object.values(this.tasks).length;
    if (Object.prototype.hasOwnProperty.call(this.tasks, id)) {
      delete this.tasks[id];
    }
   
    return Object.values(this.tasks).length !== initialLength;
  };
  
  private generateUniqueId(): UUID {
    return uuidv4() as UUID ;
  }
}
  