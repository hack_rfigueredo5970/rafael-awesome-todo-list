import { Router } from 'express';
import TaskRoutes from './routes/TaskRoutes'; 

export function createRoutes(): Router {
  const router: Router = Router();

  router.use('/tasks', TaskRoutes);

  return router;
}