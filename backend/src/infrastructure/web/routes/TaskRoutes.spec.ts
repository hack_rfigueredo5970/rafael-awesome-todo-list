import request from 'supertest';
import express, { Express, Router } from 'express';
import bodyParser from 'body-parser'; 
import { createRoutes } from '../routes';
import { ITask } from '../../../domain/entities/interfaces/ITask';
import { UUID } from '../../../utils/types/UUID';


const routes: Router = createRoutes();
const fakeDate = new Date();
const mockTask: ITask = {
  title: 'Test Task',
  description: 'Test Description',
  completed: false,
  createdBy: 'fakeUserId',
  createdAt: fakeDate,
  updatedAt: fakeDate,
};


describe('Task Routes', () => {
  let app: Express;
  let  taskId: UUID;
  beforeEach(async () => {
    app = express();
    app.use(bodyParser.json());
    app.use('/api', routes);
    const createResponse = await request(app)
      .post('/api/tasks')
      .send(mockTask);

    taskId = createResponse.body.id;
  });

  afterEach(async () => {
    app = express();
    app.use(bodyParser.json());
    app.use('/api', routes);
    await request(app).delete(`/api/tasks/${taskId}`);

    taskId = '';
  });

  it('should get all tasks', async () => {
    const response = await request(app).get('/api/tasks/');
    expect(response.status).toBe(200);
  });

  it('should get a specific task by ID', async () => {
    
 
    const taskData = { ...mockTask, title: 'New Task', completed: false, id: taskId  };
    const response = await request(app).get(`/api/tasks/${taskId}`).send(taskData);
    expect(response.status).toBe(200);
  });

  it('should create a task', async () => {
    const taskData = { ...mockTask, title: 'New Task', completed: false, id: '' }; 
    const response = await request(app).post('/api/tasks').send(taskData);
    expect(response.status).toBe(201);
  });

  it('should update a task by ID', async () => {
    const taskData = { title: 'Updated Task' }; 
    const response = await request(app).patch(`/api/tasks/${taskId}`).send(taskData);
    expect(response.status).toBe(200);
  });

  it('should delete a task by ID', async () => {
    const response = await request(app).delete(`/api/tasks/${taskId}`);
    expect(response.status).toBe(204);
  });
});
