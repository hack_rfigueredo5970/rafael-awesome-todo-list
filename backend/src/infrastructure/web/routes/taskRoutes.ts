import { Router, Request, Response } from 'express';
import { TaskController } from '../controllers/TaskController';
import { DatabaseType, createTaskRepository } from '../../database-factory/TaskRepositoryFactory';

//services
import { GetAllTasksUseCase } from '../../../application/use-cases/GetAllTasksUseCase';
import { CreateTaskUseCase } from '../../../application/use-cases/CreateTaskUseCase';
import { UpdateTaskUseCase } from '../../../application/use-cases/UpdateTaskUseCase';
import { DeleteTaskUseCase } from '../../../application/use-cases/DeleteTaskUseCase';
import { GetTaskUseCase } from '../../../application/use-cases/GetTaskUseCase';

// interfaces
import { IGetAllTaskUseCase } from '../../../application/use-cases/interfaces/IGetAllTaskUseCase';
import { ICreateTaskUseCase } from '../../../application/use-cases/interfaces/ICreateTaskUseCase';
import { IGetTaskUseCase } from '../../../application/use-cases/interfaces/IGetTaskUseCase';
import { IUpdateTaskUseCase } from '../../../application/use-cases/interfaces/IUpdateTaskUseCase';
import { IDeleteTaskUseCase } from '../../../application/use-cases/interfaces/IDeleteTaskUseCase';
import { ITaskRepository } from '../../../domain/repositories/interfaces/ITaskRepository';



const router: Router = Router();

// Retrieve the database type from your configuration or environment variable
const databaseType: DatabaseType = (process.env.DATABASE_TYPE || DatabaseType.SIMPLE) as DatabaseType;

// Create instances of your dependencies
const taskRepository: ITaskRepository = createTaskRepository(databaseType);
const getAllTaskUseCase: IGetAllTaskUseCase = new GetAllTasksUseCase(taskRepository);
const createTaskUseCase: ICreateTaskUseCase = new CreateTaskUseCase(taskRepository);
const getTaskUseCase: IGetTaskUseCase = new GetTaskUseCase(taskRepository);
const updateTaskUseCase: IUpdateTaskUseCase = new UpdateTaskUseCase(taskRepository);
const deleteTaskUseCase: IDeleteTaskUseCase  = new DeleteTaskUseCase(taskRepository);

const taskController: TaskController  = new TaskController(getAllTaskUseCase, 
  getTaskUseCase,  
  createTaskUseCase,
  updateTaskUseCase, deleteTaskUseCase);

// Get all tasks
router.get('/', async (req: Request, res: Response) => {   
  try {
    return await taskController.getAllTasks(req, res);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

// Get a specific task by ID
router.get('/:taskId', async (req: Request, res: Response) => {
  try {
    return await taskController.getTask(req, res);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});
  
// Create task
router.post('/', async (req: Request, res: Response) => {
  try {
    return await taskController.createTask(req, res); // Call the createTask method in TaskController
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

// Update a task by ID
router.patch('/:taskId', async (req: Request, res: Response) => {
  try {
    return  await taskController.updateTask(req, res);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});


// Delete a task by ID
router.delete('/:taskId', async (req: Request, res: Response) => {
  try {
    return  await taskController.deleteTask(req, res);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});
  



export default router;
