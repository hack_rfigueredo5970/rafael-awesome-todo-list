// taskController.ts

import { Request, Response } from 'express';
import { IGetAllTaskUseCase } from '../../../application/use-cases/interfaces/IGetAllTaskUseCase';
import { IGetTaskUseCase } from '../../../application/use-cases/interfaces/IGetTaskUseCase';
import { IUpdateTaskUseCase } from '../../../application/use-cases/interfaces/IUpdateTaskUseCase';
import { IDeleteTaskUseCase } from '../../../application/use-cases/interfaces/IDeleteTaskUseCase';
import { ICreateTaskUseCase } from '../../../application/use-cases/interfaces/ICreateTaskUseCase';
import { ITask } from '../../../domain/entities/interfaces/ITask';
import { UUID } from '../../..//utils/types/UUID';

export class TaskController {
  constructor(
    private readonly getAllTaskUseCase: IGetAllTaskUseCase,
    private readonly getTaskUseCase: IGetTaskUseCase,
    private readonly createTaskUseCase: ICreateTaskUseCase,
    private readonly updateTaskUseCase: IUpdateTaskUseCase,
    private readonly deleteTaskUseCase: IDeleteTaskUseCase,
    
  ) {}

  async getAllTasks(req: Request, res: Response) {
    const tasks: ITask[] = await this.getAllTaskUseCase.execute();
    res.status(200).json(tasks);
  }



  async getTask(req: Request, res: Response) {
    const taskId: UUID = req.params.taskId;
    const task: ITask | null = await this.getTaskUseCase.execute(taskId);
    if (task) {
      res.status(200).json(task);
    } else {
      res.status(404).json({ message: 'Task not found' });
    }
  }


  async createTask(req: Request, res: Response) {
    const taskData: ITask = req.body;
    if (taskData?.title) {
      taskData.title = taskData.title.trim();
    } else {
      res.status(400).json({ message: 'Missing title for task' }); 
    }
    taskData.description = taskData.description?.trim() || '';
    taskData.createdAt = new Date();
    taskData.updatedAt = new Date();
    const createdTask = await  this.createTaskUseCase.execute(taskData);
    if (createdTask) {
      res.status(201).json(createdTask);
    } else {
      res.status(404).json({ message: 'Task not found' });
    }
  }


  async updateTask(req: Request, res: Response) {
    const taskId: UUID = req.params.taskId;
    const existingTask: ITask | null = await this.getTaskUseCase.execute(taskId);
    
    if (!existingTask) {
      res.status(404).json({ message: 'Task not found' });
      return;
    }
    
    // Merge the existing task with the attributes provided in the request body
    const updatedAttributes: Partial<ITask> = req.body;
    const updatedTask: ITask = { ...existingTask, ...updatedAttributes, updatedAt: new Date() };
        
    // Update the task with the merged data
    const updatedTaskResult: ITask | null = await this.updateTaskUseCase.execute(updatedTask);
    
    if (updatedTaskResult) {
      res.status(200).json(updatedTaskResult);
    } else {
      res.status(404).json({ message: 'Task not found' });
    }
  }

  

  async deleteTask(req: Request, res: Response) {
    const taskId: UUID  = req.params.taskId;
    const isDeleted: boolean =  await this.deleteTaskUseCase.execute(taskId);
    if (isDeleted) {
      res.status(204).json({ message: 'Task deleted!' });
    } else {
      res.status(404).json({ message: 'Task not found' });
    }
  }

}
