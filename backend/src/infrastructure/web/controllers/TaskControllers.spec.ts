import { Request, Response } from 'express';
import { TaskController } from './TaskController'; 

// interfaces
import { IGetAllTaskUseCase } from '../../../application/use-cases/interfaces/IGetAllTaskUseCase';
import { IGetTaskUseCase } from '../../../application/use-cases/interfaces/IGetTaskUseCase'; 
import { IUpdateTaskUseCase } from '../../../application/use-cases/interfaces/IUpdateTaskUseCase'; 
import { ICreateTaskUseCase } from '../../../application/use-cases/interfaces/ICreateTaskUseCase'; 
import { IDeleteTaskUseCase } from '../../../application/use-cases/interfaces/IDeleteTaskUseCase'; 
import { ITask } from '../../../domain/entities/interfaces/ITask';

const fakeDate = new Date();
const mockTask: ITask = {
  title: 'Test Task',
  description: 'Test Description',
  completed: false,
  createdBy: 'fakeUserId',
  createdAt: fakeDate,
  updatedAt: fakeDate,
};

describe('TaskController', () => {
  let taskController: TaskController;
  let mockGetAllTaskUseCase: IGetAllTaskUseCase;
  let mockGetTaskUseCase: IGetTaskUseCase;
  let mockCreateTaskUseCase: ICreateTaskUseCase;
  let mockUpdateTaskUseCase: IUpdateTaskUseCase;
  let mockDeleteTaskUseCase: IDeleteTaskUseCase;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;

  beforeEach(() => {
    // Create mock instances of your use cases and dependencies
    mockGetAllTaskUseCase = {
      execute: jest.fn(),
    } as unknown as IGetAllTaskUseCase;

    mockGetTaskUseCase = {
      execute: jest.fn(),
    } as unknown as IGetTaskUseCase;

    mockCreateTaskUseCase = {
      execute: jest.fn(),
    } as unknown as ICreateTaskUseCase;

    mockUpdateTaskUseCase = {
      execute: jest.fn(),
    } as unknown as IUpdateTaskUseCase;

    mockDeleteTaskUseCase = {
      execute: jest.fn(),
    } as unknown as IDeleteTaskUseCase;

    mockRequest = {};
    mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    taskController = new TaskController(
      mockGetAllTaskUseCase,
      mockGetTaskUseCase,
      mockCreateTaskUseCase,
      mockUpdateTaskUseCase,
      mockDeleteTaskUseCase,
    );
  });

  describe('getAllTasks', () => {
    it('should return a list of tasks', async () => {
      const mockTasks: ITask[] = [{ ...mockTask, id: '1', title: 'Task 1' }, { ...mockTask, id: '2', title: 'Task 2' }];
      (mockGetAllTaskUseCase.execute as jest.Mock).mockResolvedValueOnce(mockTasks);

      await taskController.getAllTasks(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.status).toHaveBeenCalledWith(200);
      expect(mockResponse.json).toHaveBeenCalledWith(mockTasks);
    });
  });

  describe('getTask', () => {
    it('should return a task by ID', async () => {
      const taskId = '1';
      const mockTaskToRetrieve: ITask = { ...mockTask, id: taskId, title: 'Task 1' };
      (mockGetTaskUseCase.execute as jest.Mock).mockResolvedValueOnce(mockTaskToRetrieve);
      mockRequest.params = { taskId };

      await taskController.getTask(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.status).toHaveBeenCalledWith(200);
      expect(mockResponse.json).toHaveBeenCalledWith(mockTaskToRetrieve);
    });

    it('should handle task not found and return a 404 status code', async () => {
      const taskId = '1';
      (mockGetTaskUseCase.execute as jest.Mock).mockResolvedValueOnce(null);
      mockRequest.params = { taskId };

      await taskController.getTask(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.status).toHaveBeenCalledWith(404);
      expect(mockResponse.json).toHaveBeenCalledWith({ message: 'Task not found' });
    });
  });

  describe('createTask', () => {
    it('should create a new task and return a 201 status code', async () => {
      const taskData: ITask = { ...mockTask, title: 'New Task', description: 'Description' };
      const createdTask: ITask = { ...mockTask, id: '1', ...taskData };
      (mockCreateTaskUseCase.execute as jest.Mock).mockResolvedValueOnce(createdTask);
      mockRequest.body = taskData;

      await taskController.createTask(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.status).toHaveBeenCalledWith(201);
      expect(mockResponse.json).toHaveBeenCalledWith(createdTask);
    });


    it('should handle missing title and return a 400 status code', async () => {
      const taskData: ITask = { ...mockTask, description: 'Description', title: '' };
      mockRequest.body = taskData;

      await taskController.createTask(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.status).toHaveBeenCalledWith(400);
      expect(mockResponse.json).toHaveBeenCalledWith({ message: 'Missing title for task' });
    });
  });

  describe('updateTask', () => {
    it('should update a task and return a 200 status code', async () => {
      const taskId = '1';
      const updatedTaskData: Partial<ITask> = { title: 'Updated Task' };
      const updatedTask: ITask = { ...mockTask, id: taskId, ...updatedTaskData };
      (mockGetTaskUseCase.execute as jest.Mock).mockResolvedValueOnce(updatedTask);
      (mockUpdateTaskUseCase.execute as jest.Mock).mockResolvedValueOnce(updatedTask);
      mockRequest.params = { taskId };
      mockRequest.body = updatedTaskData;

      await taskController.updateTask(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.status).toHaveBeenCalledWith(200);
      expect(mockResponse.json).toHaveBeenCalledWith(updatedTask);
    });
  });

  describe('deleteTask', () => {
    it('should delete a task and return a 204 status code', async () => {
      const taskId = '1';
      (mockDeleteTaskUseCase.execute as jest.Mock).mockResolvedValueOnce(true);
      mockRequest.params = { taskId };

      await taskController.deleteTask(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.status).toHaveBeenCalledWith(204);
      expect(mockResponse.json).toHaveBeenCalledWith({ message: 'Task deleted!' });
    });

    it('should handle task not found and return a 404 status code', async () => {
      const taskId = '1';
      (mockDeleteTaskUseCase.execute as jest.Mock).mockResolvedValueOnce(false);
      mockRequest.params = { taskId };

      await taskController.deleteTask(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.status).toHaveBeenCalledWith(404);
      expect(mockResponse.json).toHaveBeenCalledWith({ message: 'Task not found' });
    });
  });
});
