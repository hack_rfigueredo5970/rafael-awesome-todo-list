
import { UUID } from '../../../utils/types/UUID';
import { ITask } from '../../entities/interfaces/ITask';

export interface ITaskRepository {
  createTask(task: ITask): Promise<ITask>;
  getTaskById(id:  UUID): Promise<ITask | null>;
  getAllTasks(): Promise<ITask[]>;
  updateTask(task: ITask): Promise<ITask | null>;
  deleteTask(id:  UUID): Promise<boolean>;
}