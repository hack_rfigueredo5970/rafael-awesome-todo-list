import { UUID } from '../../../utils/types/UUID';

export interface ITask {
  id?: UUID;
  title: string;
  description: string;
  completed: boolean;
  createdBy: string; // Add the user property to associate the task with a user.
  createdAt: Date;
  updatedAt: Date;
}
  