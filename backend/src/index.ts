import express, { Express } from 'express';
import * as dotenv from 'dotenv';
import bodyParser from 'body-parser';
import { createRoutes } from './infrastructure/web/routes';
import cors from 'cors';

dotenv.config();
const app: Express = express();
const port: number = Number(process.env.PORT) || 3000;

// Enable CORS
app.use(cors());
app.use(bodyParser.json());

const routes = createRoutes();
app.use('/api', routes);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
}); 
